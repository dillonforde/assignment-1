package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ie.cit.awd.lctutorial3.JdbcTemplateTestApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JdbcTemplateTestApplication.class)
public class LcTutorial03ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
